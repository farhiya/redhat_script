echo "Please enter your ip address: "

read ip_address

COUNTER=0
for arg in $ip_address
do
COUNTER=$(expr $COUNTER + 1)
done

if ((COUNTER == 1))
then 
    echo "This is your ip $arg" 

elif ((COUNTER < 1))
then
    echo "There was no input. Try again! "
    
    # run's the file again. This will loop until 1 input is entered
    # ./redhat_bash_script.sh

    # `exit 1` will stop bash script
    exit 1
elif ((COUNTER > 1))
then

    echo "That was too many inputs. Try again! "
    #To run script again ->  ./redhat_bash_script.sh
    exit 1

fi

# opening ec2 linux machine and removing fingerprint check at given ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ec2-user@$ip_address '

# Update current packages
sudo yum update -y

if sudo httpd.x86_64 -V;
then
echo 'Apache already installed'
else
  # Install Apache Web Server
    sudo yum install -y httpd.x86_64  
fi

'

# saves the bash script in redhat
`scp -i ~/.ssh/ch9_shared.pem redhat_bash_script.sh ec2-user@$ip_address:/home/ec2-user`

# saves the website folder in redhat
`scp -i ~/.ssh/ch9_shared.pem -r website ec2-user@$ip_address:/home/ec2-user`

# opening ec2 linux machine and removing fingerprint check at given ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ec2-user@$ip_address '

chmod +x redhat_bash_script.sh


# Start Apache Server
sudo systemctl start httpd.service

# auto start system on boot
sudo systemctl enable httpd.service

# Move the file into the correct location
sudo mv ~/website/index.html /var/www/html/index.html

# Restart Apache
sudo systemctl restart httpd.service 
'

scp -i ~/.ssh/ch9_shared.pem -r website ec2-user@$ip_address:/home/ec2-user

echo "apache setup complete"