# REDHAT README

In this README there is a step by step guide to create and run a bash script in REDHAT. You will download Apache and replace the generic web page with your webpage.

1. Inside the bash terminal where you want to create the folder:

      - Create a new folder for REDHAT scripting
         - ` mkdir REDHAT`

   - Go into REDHAT folder and create a README.md, a bash script for the apache download and a folder with your website.

        - `touch README.md redhat_bash_script.sh`
        - `mkdir website`

    - Inside the website folder create an index.html file that you want replace the generic apache welcome page with.

      - `cd website`
      - `touch index.html`

    - Connect to your repo

2. Update files in VSCode
   
    - Configure you're bash script with the following:
  
        ```bash
            # Update current packages
            sudo yum update -y

            # Install Apache Web Server
            sudo yum install -y httpd.x86_64

            # Start Apache Server
            sudo systemctl start httpd.service

            # auto start system on boot
            sudo systemctl enable httpd.service

            # Move the file into the correct location
            sudo mv ~/website/index.html /var/www/html/index.html

            # Restart Apache
            sudo systemctl restart httpd.service 

        ```
    - Configure your html file
    - Save all files

3. Connect to REDHAT in a new terminal
   
    Make sure you use your own IP address and your own pem
   
    - `ssh -i ~/.ssh/ch9_shared.pem ec2-user@34.248.42. 110`
    
    You are now in a linux terminal. At this point we should have 2 seperate terminal windows. One for bash and one for linux. This will make the next few stages easier.

4. Moving files and folders from local to linux machine
   
   This should be done in our BASH terminal

        ```bash

        # saves the bash script in redhat
        `scp -i ~/.ssh/ch9_shared.pem redhat_bash_script.sh ec2-user@34.248.42.110:/home/ec2-user`

        # saves the website folder in redhat
        `scp -i ~/.ssh/ch9_shared.pem -r website ec2-user@34.248.42.110:/home/ec2-user`

        ```

5. In LINUX terminal, give the bash script correct permissions
   
    - Give permissions for to the bash file 
        `chmod +x redhat_bash_script.sh`

6. In the BASH terminal run the bash script in the background in redhat 

    - This will run bash and your new html will replace the generic Apache web page

        `ssh -i ~/.ssh/ch9_shared.pem ec2-user@34.248.42.110 ./redhat_bash_script.sh`

7. Go to the ip address and your new page should appear.
    - In the web browser : 34.248.42.110

8. Save everything in your repo. Use the BASH terminal.

